import requests
import os

api_key = ""

def download_dem(route):

    print(f"Attempting to download {route}")

    demtype = "COP30"
    output_format = "GTiff"

    north = route["bbox"][0][1]
    south = route["bbox"][1][1]
    west = route["bbox"][0][0]
    east = route["bbox"][1][0]

    params = f"API_Key={api_key}&demtype={demtype}&south={south}&north={north}&west={west}&east={east}&outputFormat={output_format}"

    url = f"https://portal.opentopography.org/API/globaldem?{params}"
    print("Requesting…")
    route_filename = route["filename"]
    fname = os.path.join("generated", "dem", f"{route_filename}_dem.tif")

    print(f"Filename: {fname}")

    if not os.path.isfile(fname):
        response = requests.get(url)
        print("Writing file…")
        open(fname, 'wb').write(response.content)
    else:
        print("File exists")

    print("Done.", end="\n\n\n")
