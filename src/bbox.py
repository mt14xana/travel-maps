def generate_bbox(x1, y1, x2, y2, margin=0.2):

    lon1 = min(x1, x2)
    lat1 = min(y1, y2)

    lon2 = max(x1, x2)
    lat2 = max(y1, y2)

    lon1 -= margin
    lat1 -= margin
    lon2 += margin
    lat2 += margin
    
    edge_length = max(
        abs(lon1 - lon2),
        abs(lat1 - lat2)
    )

    center_lon = min(lon1, lon2) + abs(lon1 - lon2) / 2
    center_lat = min(lat1, lat2) + abs(lat1 - lat2) / 2

    lon_out1 = center_lon - edge_length / 2
    lat_out1 = center_lat - edge_length / 2

    lon_out2 = center_lon + edge_length / 2
    lat_out2 = center_lat + edge_length / 2 

    return (
        [  # top left
            min(lon_out1, lon_out2),
            max(lat_out1, lat_out2)
        ],
        [  # bottom right
            max(lon_out1, lon_out2),
            min(lat_out1, lat_out2)
        ]
    )