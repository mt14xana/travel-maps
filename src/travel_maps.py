import sys
import os
import json
import subprocess
import satimg
import dem

"""
Requires Rscript
"""

""" Makes sure that all required folders exist """
def prepare_folders():

    if not os.path.isdir("generated"):
        os.mkdir("generated")
    
    if not os.path.isdir(os.path.join("generated", "dem")):
        os.mkdir(os.path.join("generated", "dem"))
    
    if not os.path.isdir(os.path.join("generated", "img")):
        os.mkdir(os.path.join("generated", "img"))

    if not os.path.isdir(os.path.join("generated", "assets_3D")):
        os.mkdir(os.path.join("generated", "assets_3D"))

    if not os.path.isdir(os.path.join("generated", "render")):
        os.mkdir(os.path.join("generated", "render"))
    

""" Loads route file """
def load_route_settings_from_file(fpath):

    route_settings = {};

    with open(fpath, "r") as fp:
        route_settings = json.load(fp)

    return route_settings


""" Prepares route by calculating bouding box and filename 
    and downloading DEM and satellite images """
def prepare_route(route_settings):

    if route_settings["generate_bbox"]:
        import bbox
        # [top left, bottom right] bbox from first and last coordinates
        route_settings["bbox"] = bbox.generate_bbox(
            route_settings["coords"][0][0],
            route_settings["coords"][0][1],
            route_settings["coords"][-1][0],
            route_settings["coords"][-1][1]
        )
    
    if route_settings["generate_filename"]:
        route_settings["filename"] = route_settings["name"]

    if "-s" not in sys.argv:
        dem.download_dem(route_settings)
        satimg.download_img(route_settings, zoom=11, api_key="")
    generate_assets(route_settings)


""" Runs asset generation scripts in R """
def generate_assets(route_settings):
    print(json.dumps(route_settings))
    if "-s" not in sys.argv:
        subprocess.run(["Rscript", "generate_cost.r", json.dumps(route_settings)])
        subprocess.run(["Rscript", "generate_assets.r", json.dumps(route_settings)])


def main():

    if len(sys.argv) > 1:

        dirname = os.path.dirname(__file__)
        filename = os.path.join(dirname, os.path.join(dirname, sys.argv[1]) )

        if os.path.isfile(filename):
            prepare_folders()
            route_settings = load_route_settings_from_file(filename)

            prepare_route(route_settings)
        else:
            print("File does not exist:", filename)

    else:
        exit();


if __name__ == "__main__":
    main();