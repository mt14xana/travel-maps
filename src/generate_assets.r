library(rjson)
library(rayshader)
library(sp)
library(raster)
library(scales)
library(rgdal)
library(stringr)

library(rgeos)
library(Matrix)
library(gdistance)
library(leastcostpath)
setwd('/home/marv/Projects/travel-maps')

args = commandArgs(trailingOnly=TRUE)

print(args)



# Route settings
json_data <- fromJSON(str_replace(args[1], fixed("\\"), ""))
# json_data <- fromJSON('{"name": "Kerman to Cobinan (Kuhbanan)", "coords": [[57.0662499, 30.27305095000001], [56.28006, 31.41262949999999]], "generate_bbox": true, "bbox": [[55.903365675, 31.61262949999999], [57.44294422499999, 30.07305095000001]], "generate_filename": true, "filename": "Kerman to Cobinan (Kuhbanan)"}')
# json_data <- fromJSON('{"name": "Cobinan (Kuhbanan) to Tonocain (Tabas)", "coords": [[56.28006, 31.41262949999999], [56.94565779999999, 33.60953795]], "generate_bbox": true, "bbox": [[55.314404674999984, 33.809537950000006], [57.911313125, 31.21262949999999]], "bbox_margin": 0.2, "generate_filename": true, "filename": "Cobinan (Kuhbanan) to Tonocain (Tabas)"}')

print(json_data$name)

filename <- json_data$filename


elevation <- raster::raster(
    paste("generated/dem/", filename, "_dem.tif", sep = "")
)

    # Extent of route
print(json_data$bbox)
north <- json_data$bbox[[1]][[2]]
east <- json_data$bbox[[2]][[1]]
south <- json_data$bbox[[2]][[2]]
west <- json_data$bbox[[1]][[1]]
top_right <- c(y = east, x = north)
bottom_left <- c(y = west, x = south)


max_length <- 256
dims <- dim(elevation)

ratio <- min(max_length / dims[1], max_length / dims[2]);

s <- raster(nrow = dims[1] * ratio, ncol = dims[2] * ratio)
extent(s) <- extent(elevation)
elevation <- resample(elevation, s, method = 'bilinear') # resample output

elevation_matrix <- rayshader::raster_to_matrix(elevation)


top_right <- c(y = east, x = north)
bottom_left <- c(y = west, x = south)
extent_bbox <- extent(
    sp::SpatialPoints(
        rbind(bottom_left, top_right),
        proj4string = sp::CRS(
            attr(attr(elevation, "crs"), "projargs")
        )
    )
)

# Sets projected extent, necessary for correctly displaying route
attr(elevation, "extent") <- extent_bbox

# Loads hillshade
map_rgb <- raster::brick(
    paste("generated/img/", filename, "_img.tif", sep = "")
)



# Projects hillshade onto elevation crs
# map_rgb <- raster::projectRaster(map_rgb_3857, crs = crs(elevation), method = "bilinear")
map_rgb <- raster::crop(map_rgb, extent_bbox)

# map_rgb

# plotRGB(map_rgb)
# Prepares hillshade array
print("preparing img")
names(map_rgb) <- c("r","g","b")
satellite_imagery_mask_r <- rayshader::raster_to_matrix(map_rgb$r)
satellite_imagery_mask_g <- rayshader::raster_to_matrix(map_rgb$g)
satellite_imagery_mask_b <- rayshader::raster_to_matrix(map_rgb$b)


sentinel_mask_array <- array(
    0,
    dim=c(nrow(satellite_imagery_mask_r), ncol(satellite_imagery_mask_r), 3))
sentinel_mask_array[, , 1] <- satellite_imagery_mask_r / 255 #Red layer
sentinel_mask_array[, , 2] <- satellite_imagery_mask_g / 255 #Blue layer
sentinel_mask_array[, , 3] <- satellite_imagery_mask_b / 255 #Green layer

sentinel_mask_array <- aperm(sentinel_mask_array, c(2, 1, 3))

# render_snapshot(clear = TRUE)

print("plotting")

plot_3d(
    sentinel_mask_array,
    elevation_matrix,
    zscale = 100,
    zoom = 0.5,
    phi = 45,
    theta = -45,
    fov = 70,
    background = "#F2E1D0",
    shadowcolor = "#523E2B",
    solid = TRUE
)


render_scalebar(limits=c(0, 10, 20), scale_length = c(0, 0.5), segments = 2,  label_unit = "km")
    
print("saving obj")
# saves obj
save_obj(paste("generated/render/", filename, "_", ".obj", sep = ""))

print("saving height map")
# saves heightmap
shade <- height_shade(elevation_matrix, texture = gray.colors(256))
save_png(shade, paste("generated/render/", filename, "_height", ".png", sep = ""))

shade <- ambient_shade(elevation_matrix)
save_png(shade, paste("generated/render/", filename, "_ao", ".png", sep = ""), rotate = 180)



