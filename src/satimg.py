import math
import urllib.request
import os
import glob
import subprocess
import shutil
from tile_convert import bbox_to_xyz, tile_edges
from osgeo import gdal

# source: https://jimmyutterstrom.com/blog/2019/06/05/map-tiles-to-geotiff/

def fetch_tile(x, y, z, tile_source):
    url = tile_source.replace(
        "{x}", str(x)).replace(
        "{y}", str(y)).replace(
        "{z}", str(z))
    path = os.path.join("temp", f"{x}_{y}_{z}.png")
    urllib.request.urlretrieve(url, path)
    return(path)


def merge_tiles(input_pattern, output_path):
    merge_command = ['gdal_merge.py', '-o', output_path]

    for name in glob.glob(input_pattern):
        merge_command.append(name)

    subprocess.call(merge_command)


def georeference_raster_tile(x, y, z, path):

    bounds = tile_edges(x, y, z)
    filename, extension = os.path.splitext(path)
    gdal.Translate(filename + '.tif',
                   path,
                   outputSRS='EPSG:4326',
                   outputBounds=bounds)


def download_img(route, zoom=16, api_key=""):

    fname = os.path.join(os.path.dirname(__file__), "generated", "img", route["filename"] + "_img.tif")

    if os.path.isfile(fname): 
        print("File exists")
        return

    if not os.path.isdir("temp"):
        os.makedirs("temp")
    tile_source = "https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=" + api_key

    # temp_dir = os.path.join(os.path.dirname(__file__), 'temp')
    output_dir = os.path.join("generated", "img")
    lon_min = route["bbox"][0][0]
    lon_max = route["bbox"][1][0]
    lat_min = route["bbox"][1][1]
    lat_max = route["bbox"][0][1]

    x_min, x_max, y_min, y_max = bbox_to_xyz(
    lon_min, lon_max, lat_min, lat_max, zoom)

    print(f"Fetching {(x_max - x_min + 1) * (y_max - y_min + 1)} tiles")


    for x in range(x_min, x_max + 1):
        for y in range(y_min, y_max + 1):
            try:
                png_path = fetch_tile(x, y, zoom, tile_source)
                print(f"{x},{y} fetched")
                georeference_raster_tile(x, y, zoom, png_path)
            except OSError:
                print(f"{x},{y} missing")
                pass

    print("Fetching of tiles complete")

    print("Merging tiles")
    print(os.path.join("tmp", "*.tif"))
    print(fname)
    merge_tiles(os.path.join(os.path.dirname(__file__), "temp", "*.tif"), fname)
    print("Merge complete")

    shutil.rmtree("temp")

