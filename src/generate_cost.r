library(rjson)
library(rayshader)
library(sp)
library(raster)
library(scales)
library(rgdal)
library(stringr)
library(Thermimage)
library(rgeos)
library(Matrix)
library(gdistance)
library(leastcostpath)
setwd('/home/marv/Projects/travel-maps')

args = commandArgs(trailingOnly=TRUE)

print(args)


# Route settings
json_data <- fromJSON(str_replace(args[1], fixed("\\"), ""))
#json_data <- fromJSON('{"name": "Kerman to Cobinan (Kuhbanan)", "coords": [[57.0662499, 30.27305095000001], [56.28006, 31.41262949999999]], "generate_bbox": true, "bbox": [[55.903365675, 31.61262949999999], [57.44294422499999, 30.07305095000001]], "generate_filename": true, "filename": "Kerman to Cobinan (Kuhbanan)"}')
# json_data <- fromJSON('{"name": "Cobinan (Kuhbanan) to Tonocain (Tabas)", "coords": [[56.28006, 31.41262949999999], [56.94565779999999, 33.60953795]], "generate_bbox": true, "bbox": [[55.314404674999984, 33.809537950000006], [57.911313125, 31.21262949999999]], "bbox_margin": 0.2, "generate_filename": true, "filename": "Cobinan (Kuhbanan) to Tonocain (Tabas)"}')


print(json_data$name)

filename <- json_data$filename


elevation <- raster::raster(
    paste("generated/dem/", filename, "_dem.tif", sep = "")
)

# Extent of route
print(json_data$bbox)
north <- json_data$bbox[[1]][[2]]
east <- json_data$bbox[[2]][[1]]
south <- json_data$bbox[[2]][[2]]
west <- json_data$bbox[[1]][[1]]
top_right <- c(y = east, x = north)
bottom_left <- c(y = west, x = south)

max_length <- 1024

ratio <- min(max_length / nrow(elevation), max_length / ncol(elevation))

s <- raster(nrow = nrow(elevation) * ratio, ncol = ncol(elevation) * ratio) # 4096 does not work with 64gb ram
extent(s) <- extent(elevation)
elevation <- resample(elevation, s, method = 'bilinear') # resample output
elevation_matrix <- rayshader::raster_to_matrix(elevation)


top_right <- c(y = east, x = north)
bottom_left <- c(y = west, x = south)
extent_bbox <- extent(
    sp::SpatialPoints(
        rbind(bottom_left, top_right),
        proj4string = sp::CRS(
            attr(attr(elevation, "crs"), "projargs")
        )
    )
)

A <-  sp::SpatialPoints(
        cbind(json_data$coords[[1]][[1]], json_data$coords[[1]][[2]]),
        proj4string = sp::CRS(
            attr(attr(elevation, "crs"), "projargs")
        )
    )

B <-  sp::SpatialPoints(
        cbind(json_data$coords[[-1]][[1]], json_data$coords[[-1]][[2]]),
        proj4string = sp::CRS(
            attr(attr(elevation, "crs"), "projargs")
        )
    )





attr(elevation, "extent") <- extent_bbox

slope_cs <- create_slope_cs(elevation, cost_function = 'tobler', max_slope = 30, neighbours = 32, exaggeration = TRUE)

lcp <- leastcostpath::create_lcp(cost_surface = slope_cs, origin = A, destination = B, directional = FALSE)

cc <- create_cost_corridor(slope_cs, A, B, rescale = TRUE)

cc <- rayshader::raster_to_matrix(cc)

#save_png(mirror.matrix(cc), paste("generated/render/", json_data$filename, "_cost_corridor.png"))

plot(raster(slope_cs), col = grey.colors(1, start=0.9, end=0.9))
#plot(cc, col = grey.colors(255, start=0, end=1))
plot(A, add = T, col = "black")
plot(B, add = T, col = "black")
# LCP from A-to-B
plot(lcp[1,], add = T, col = "red")
# LCP from B-to-A
#plot(lcp[2,], add = T, col = "blue")
